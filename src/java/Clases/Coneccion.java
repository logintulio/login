/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Pazos
 */
public class Coneccion {
    private Connection cnn;
    
    public Coneccion(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String db="jdbc:mysql://localhost/login";
            cnn=DriverManager.getConnection(db,"root","");
        }catch(ClassNotFoundException | SQLException ex){
            Logger.getLogger(Coneccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cerrar(){
        try{
            cnn.close();
        }catch(SQLException ex){
            Logger.getLogger(Coneccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean acceder(String usuario, String pass){
        try {
            String sql="select (1) from usuarios where usuario='"+usuario+"' and pass='"+pass+"';";
            Statement st=cnn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(Coneccion.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public void registrar(String usuario, String pass){
        try {
            String sql="insert into usuarios(usuario,pass) values('"+usuario+"','"+pass+"');";
            Statement st=cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Coneccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
